package com.example.stv.myapplication.TopStories.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.android.volley.toolbox.NetworkImageView;
import com.example.stv.myapplication.AppDelegate;
import com.example.stv.myapplication.R;


public class NewsArticleFragment extends Fragment {


    private final String mimeType = "text/html";
    private final String encoding = "UTF-8";


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_article, container, false);
        //get bundle data from HomeFragment listItem click
        String html = getArguments().getString(HomeFragment.storiesContentHtmlKey);
        String imageUrl=getArguments().getString(HomeFragment.storiesImageUrlKey);

        NetworkImageView imageView= (NetworkImageView)rootView.findViewById(R.id.imagetopstories);
        // Feed image
        if (!TextUtils.isEmpty(imageUrl)) {
            imageView.setImageUrl(imageUrl, AppDelegate.getInstance().getImageLoader());
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }
        WebView browser = (WebView) rootView.findViewById(R.id.articleWebView);
        browser.loadDataWithBaseURL("",html,mimeType,encoding,"");
        return rootView;
    }

}


