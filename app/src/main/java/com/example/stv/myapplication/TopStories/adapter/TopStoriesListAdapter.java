package com.example.stv.myapplication.TopStories.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.stv.myapplication.AppDelegate;
import com.example.stv.myapplication.R;
import com.example.stv.myapplication.TopStories.model.TopStoriesItem;

import java.util.List;

/**
 * Used for ListView on Main Page
 */
public class TopStoriesListAdapter extends BaseAdapter implements View.OnClickListener {
    private Activity activity;
    private LayoutInflater inflater;
    private List<TopStoriesItem> storiesItems;
    ImageLoader imageLoader = AppDelegate.getInstance().getImageLoader();
    private OnStoriesItemClickListener onStoriesItemClickListener;
    private static final String LOG_TAG = TopStoriesListAdapter.class.getSimpleName();

    public TopStoriesListAdapter(Activity activity, List<TopStoriesItem> storiesItems){
        this.activity = activity;
        this.storiesItems = storiesItems;
    }

    public List<TopStoriesItem> getDataItems() {
        return storiesItems;
    }

    public void setStoriesItems(List<TopStoriesItem> storiesItems) {
        this.storiesItems = storiesItems;
    }

    @Override
    public int getCount() {
        return storiesItems.size();
    }

    @Override
    public Object getItem(int location) {
        return storiesItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Avoid unnecessary calls to findViewById() on each row, which is expensive!
        ViewHolder holder;

        if (inflater == null) inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.topstories_item, null);
            holder = new ViewHolder();
            holder.feedItemContainer = (RelativeLayout)convertView.findViewById(R.id.feedItemContainer);
            holder.title=(TextView)convertView.findViewById(R.id.title);
            holder.subTitle=(TextView)convertView.findViewById(R.id.subtitle);
            holder.feedImageView = (NetworkImageView) convertView.findViewById(R.id.thumbnailtopstories);
            convertView.setTag(holder);
        }
        else {
            // Get the ViewHolder back to get fast access to the widgets.
            holder = (ViewHolder) convertView.getTag();
        }
        if (imageLoader == null)
            imageLoader = AppDelegate.getInstance().getImageLoader();

        holder.feedItemContainer.setTag(position);
        holder.feedItemContainer.setOnClickListener(this);

        //get TopStoriesItem based on listView position
        TopStoriesItem item = storiesItems.get(position);
        //set source TextView i.e Upper right corner of the item row.
        holder.title.setText(item.getTitle());
        holder.subTitle.setText(item.getSubHeadline());
        // Feed image
        if (!TextUtils.isEmpty(item.getImageURL())) {
            holder.feedImageView.setImageUrl(item.getImageURL(), imageLoader);
            holder.feedImageView.setVisibility(View.VISIBLE);
        } else {
            holder.feedImageView.setVisibility(View.GONE);
        }



        return convertView;
    }


    @Override
    public void onClick(View view) {
        final int viewId = view.getId();
        if (viewId == R.id.feedItemContainer) {
            if (onStoriesItemClickListener != null) {
                Log.d(LOG_TAG, "OnClickContainer, position is :" + (Integer) view.getTag());
                onStoriesItemClickListener.onFeedItemClick(view, (Integer) view.getTag());
            }
        }


    }


    static class ViewHolder{
        RelativeLayout feedItemContainer;
        TextView title,subTitle ;
        NetworkImageView feedImageView ;

    }

    public interface OnStoriesItemClickListener {

        public void onFeedItemClick(View v, int position);
    }

    public void setOnStoriesItemClickListener(OnStoriesItemClickListener onStoriesItemClickListener) {
        this.onStoriesItemClickListener = onStoriesItemClickListener;
    }


}
