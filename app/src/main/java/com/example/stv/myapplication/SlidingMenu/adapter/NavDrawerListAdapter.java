package com.example.stv.myapplication.SlidingMenu.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.stv.myapplication.R;
import com.example.stv.myapplication.SlidingMenu.model.NavigationalDrawerItem;

import java.util.ArrayList;

public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<NavigationalDrawerItem> navDrawerItems;

	
	public NavDrawerListAdapter(Context context, ArrayList<NavigationalDrawerItem> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
       	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        // Avoid unneccessary calls to findViewById() on each row, which is expensive!
        ViewHolder holder;
        /*
         * If convertView is not null, we can reuse it directly, no inflation required!
         * We only inflate a new View when the convertView is null.
         */
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
            holder = new ViewHolder();
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.icon);
            holder.txTitle = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(holder);

             }

        else {
            // Get the ViewHolder back to get fast access to the TextView and the ImageView.
            holder = (ViewHolder) convertView.getTag();
        }
         
         // Bind that data efficiently!
        holder.imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
        holder.txTitle.setText(navDrawerItems.get(position).getTitle());

        return convertView;
	}


    static class ViewHolder{
        TextView txTitle;
        ImageView imgIcon;
    }


}
