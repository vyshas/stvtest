package com.example.stv.myapplication.TopStories.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.stv.myapplication.AppDelegate;
import com.example.stv.myapplication.R;
import com.example.stv.myapplication.TopStories.adapter.TopStoriesListAdapter;
import com.example.stv.myapplication.TopStories.model.TopStoriesItem;
import com.example.stv.myapplication.Utility.GsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class HomeFragment extends Fragment implements TopStoriesListAdapter.OnStoriesItemClickListener  {

    private String URL_FEED = "http://api.stv.tv/articles/?count=50&navigationLevelId=1218&orderBy=ranking+DESC%2C+createdAt+DESC&full=1&count=50";
    private static final String LOG_TAG = HomeFragment.class.getSimpleName();
    //intent key for sharing contentHTML between fragments
    protected static String storiesContentHtmlKey ="storiesItemContentHTML";
    //intent key for sharing imageURL between fragments
    protected static String storiesImageUrlKey="storiesItemImageUrl";
    //list of topStories ojects
    private List<TopStoriesItem> topStoriesItems ;
    private ListView topStorieslistView;
    private TopStoriesListAdapter topStorieslistAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    //fragment for displaying each news article
    private Fragment newsArticleFragment;


    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initialize list for data items
        topStoriesItems = new ArrayList<TopStoriesItem>();
        //set data items list for Listview
        topStorieslistAdapter = new TopStoriesListAdapter(getActivity(), topStoriesItems);
        if (newsArticleFragment == null) {
            newsArticleFragment = new NewsArticleFragment();
        }

        topStorieslistAdapter.setOnStoriesItemClickListener(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.topstories_listview, container, false);
        //initialize listView
        topStorieslistView = (ListView) rootView.findViewById(R.id.v);
        topStorieslistView.setAdapter(topStorieslistAdapter);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.refresh_arrow);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.refresh_bg);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                   getFromNetwork();
            }
        });
       getFromNetwork();
        return rootView;
    }


    /**
     * Function used for requesting data from network (not from cache)
     */
    public void getFromNetwork() {

        topStorieslistAdapter.getDataItems().clear();
        mSwipeRefreshLayout.setRefreshing(true);
        GsonRequest<TopStoriesItem[]> gsonRequest =
                new GsonRequest<TopStoriesItem[]>(URL_FEED, TopStoriesItem[].class,

                        new Response.Listener<TopStoriesItem[]>() {
                            @Override
                            public void onResponse(TopStoriesItem[] response) {
                                // Arrays.asList gives an umodifiable wrapper ,so use a linked list to overcome that
                                topStoriesItems = new LinkedList( Arrays.asList(response));
                                for (TopStoriesItem topStoriesItem : topStoriesItems) {
                                     getImageURL(topStoriesItem);
                                }

                                topStorieslistAdapter.setStoriesItems(topStoriesItems);
                                // notify changes to list adapater
                                topStorieslistAdapter.notifyDataSetChanged();

                            }

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(LOG_TAG, "Error: " + error.getMessage());
                        if (error instanceof NoConnectionError)
                            Log.e(LOG_TAG, "No internet Access, Check your internet connection.");
                       else if (error instanceof ParseError) {
                            Log.e(LOG_TAG, "Parse ERROR");
                            Toast.makeText(getActivity().getApplicationContext(), "Parse Error.INVALID Data", Toast.LENGTH_SHORT).show();
                        }
                        else
                            Log.e(LOG_TAG, "Error Retrieving Request/n" + error.getMessage());
                    }
                });


        mSwipeRefreshLayout.setRefreshing(false);
        // Adding request to volley request queue
        AppDelegate.getInstance().addToRequestQueue(gsonRequest);

    }

    /**
     * gets ImageURL for each story item
     * @param storiesItem
     */
protected void getImageURL(final TopStoriesItem storiesItem)
{

    String imageURL="http://api.stv.tv/images/"+storiesItem.getImages().get(0).getId()+"/rendition/?width=640&height=360";
    JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, imageURL, null, new Response.Listener<JSONObject>() {

        @Override
        public void onResponse(JSONObject response) {
            //SUCCESS
            VolleyLog.d(LOG_TAG, "Response: " + response.toString());
            if (response != null) {
                try {
                    String imageURL=response.getString("url");
                    //Log.d(LOG_TAG,"Image URL="+imageURL);
                    storiesItem.setImageURL(imageURL);
                    topStorieslistAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    }, new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError error) {
            //ERROR
            VolleyLog.d(LOG_TAG, "Error: " + error.getMessage());
            Log.d(LOG_TAG, "getFeed():Not cacheHit, ERROR:" + error.getMessage());
            if (error instanceof NoConnectionError)
                Log.e(LOG_TAG, "No internet Access, Check your internet connection.");

            else
                Log.e(LOG_TAG, "Error Retrieving Request/n" + error.getMessage());

        }
    });


// Adding request to volley request queue
    AppDelegate.getInstance().addToRequestQueue(jsonReq);

}


    public void scrollListToTop()
    {
        topStorieslistView.setSelectionAfterHeaderView();
    }


    @Override
    public void onFeedItemClick(View v, int position) {
        //selected item
        TopStoriesItem storiesItem = (TopStoriesItem)topStorieslistAdapter.getItem(position);
        Bundle bundle = new Bundle();
        bundle.putString(storiesContentHtmlKey, storiesItem.getContentHTML());
        bundle.putString(storiesImageUrlKey,storiesItem.getImageURL());
        newsArticleFragment.setArguments(bundle);

        android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, newsArticleFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
}
