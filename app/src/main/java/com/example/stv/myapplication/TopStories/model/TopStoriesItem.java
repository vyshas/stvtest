
package com.example.stv.myapplication.TopStories.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TopStoriesItem {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("publishDate")
    @Expose
    private String publishDate;
    @SerializedName("shortHeadline")
    @Expose
    private String shortHeadline;
    @SerializedName("subHeadline")
    @Expose
    private String subHeadline;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("contentHTML")
    @Expose
    private String contentHTML;
    @SerializedName("contentMarkdown")
    @Expose
    private Object contentMarkdown;
    @SerializedName("contentType")
    @Expose
    private String contentType;
    @SerializedName("permalinkNavigationId")
    @Expose
    private String permalinkNavigationId;
    @SerializedName("permalink")
    @Expose
    private String permalink;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("modifiedAt")
    @Expose
    private String modifiedAt;
    @SerializedName("creatorUserId")
    @Expose
    private String creatorUserId;
    @SerializedName("modifierUserId")
    @Expose
    private String modifierUserId;
    @SerializedName("bylineUserId")
    @Expose
    private Object bylineUserId;
    @SerializedName("publisher")
    @Expose
    private String publisher;
    @SerializedName("seoTitle")
    @Expose
    private String seoTitle;
    @SerializedName("relatedItems")
    @Expose
    private Object relatedItems;
    @SerializedName("syndicate")
    @Expose
    private String syndicate;
    @SerializedName("mobileOnly")
    @Expose
    private String mobileOnly;
    @SerializedName("navigation")
    @Expose
    private HashMap<String,Integer>  navigation;
    @SerializedName("topics")
    @Expose
    private List<Object> topics = new ArrayList<Object>();
    @SerializedName("tags")
    @Expose
    private List<Object> tags = new ArrayList<Object>();
    @SerializedName("meta")
    @Expose
    private HashMap<String,String> meta;
    @SerializedName("videos")
    @Expose
    private List<Object> videos = new ArrayList<Object>();
    @SerializedName("images")
    @Expose
    private List<Image> images = new ArrayList<Image>();
    @SerializedName("geolocations")
    @Expose
    private List<Object> geolocations = new ArrayList<Object>();
    @SerializedName("relatedArticles")
    @Expose
    private List<Object> relatedArticles = new ArrayList<Object>();


    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    private String imageURL;

    /**
     * No args constructor for use in serialization
     * 
     */
    public TopStoriesItem() {
    }

    /**
     * 
     * @param modifiedAt
     * @param permalinkNavigationId
     * @param mobileOnly
     * @param contentType
     * @param subHeadline
     * @param contentMarkdown
     * @param meta
     * @param publisher
     * @param id
     * @param syndicate
     * @param seoTitle
     * @param title
     * @param creatorUserId
     * @param description
     * @param createdAt
     * @param modifierUserId
     * @param relatedArticles
     * @param publishDate
     * @param tags
     * @param status
     * @param relatedItems
     * @param videos
     * @param contentHTML
     * @param geolocations
     * @param guid
     * @param topics
     * @param permalink
     * @param navigation
     * @param images
     * @param shortHeadline
     * @param bylineUserId
     */
    public TopStoriesItem(String id, String guid, String title, String publishDate, String shortHeadline, String subHeadline, Object description, String contentHTML, Object contentMarkdown, String contentType, String permalinkNavigationId, String permalink, String status, String createdAt, String modifiedAt, String creatorUserId, String modifierUserId, Object bylineUserId, String publisher, String seoTitle, Object relatedItems, String syndicate, String mobileOnly, HashMap<String,Integer> navigation, List<Object> topics, List<Object> tags, HashMap<String,String> meta, List<Object> videos, List<Image> images, List<Object> geolocations, List<Object> relatedArticles) {
        this.id = id;
        this.guid = guid;
        this.title = title;
        this.publishDate = publishDate;
        this.shortHeadline = shortHeadline;
        this.subHeadline = subHeadline;
        this.description = description;
        this.contentHTML = contentHTML;
        this.contentMarkdown = contentMarkdown;
        this.contentType = contentType;
        this.permalinkNavigationId = permalinkNavigationId;
        this.permalink = permalink;
        this.status = status;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
        this.creatorUserId = creatorUserId;
        this.modifierUserId = modifierUserId;
        this.bylineUserId = bylineUserId;
        this.publisher = publisher;
        this.seoTitle = seoTitle;
        this.relatedItems = relatedItems;
        this.syndicate = syndicate;
        this.mobileOnly = mobileOnly;
        this.navigation = navigation;
        this.topics = topics;
        this.tags = tags;
        this.meta = meta;
        this.videos = videos;
        this.images = images;
        this.geolocations = geolocations;
        this.relatedArticles = relatedArticles;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The guid
     */
    public String getGuid() {
        return guid;
    }

    /**
     * 
     * @param guid
     *     The guid
     */
    public void setGuid(String guid) {
        this.guid = guid;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The publishDate
     */
    public String getPublishDate() {
        return publishDate;
    }

    /**
     * 
     * @param publishDate
     *     The publishDate
     */
    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    /**
     * 
     * @return
     *     The shortHeadline
     */
    public String getShortHeadline() {
        return shortHeadline;
    }

    /**
     * 
     * @param shortHeadline
     *     The shortHeadline
     */
    public void setShortHeadline(String shortHeadline) {
        this.shortHeadline = shortHeadline;
    }

    /**
     * 
     * @return
     *     The subHeadline
     */
    public String getSubHeadline() {
        return subHeadline;
    }

    /**
     * 
     * @param subHeadline
     *     The subHeadline
     */
    public void setSubHeadline(String subHeadline) {
        this.subHeadline = subHeadline;
    }

    /**
     * 
     * @return
     *     The description
     */
    public Object getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(Object description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The contentHTML
     */
    public String getContentHTML() {
        return contentHTML;
    }

    /**
     * 
     * @param contentHTML
     *     The contentHTML
     */
    public void setContentHTML(String contentHTML) {
        this.contentHTML = contentHTML;
    }

    /**
     * 
     * @return
     *     The contentMarkdown
     */
    public Object getContentMarkdown() {
        return contentMarkdown;
    }

    /**
     * 
     * @param contentMarkdown
     *     The contentMarkdown
     */
    public void setContentMarkdown(Object contentMarkdown) {
        this.contentMarkdown = contentMarkdown;
    }

    /**
     * 
     * @return
     *     The contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * 
     * @param contentType
     *     The contentType
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * 
     * @return
     *     The permalinkNavigationId
     */
    public String getPermalinkNavigationId() {
        return permalinkNavigationId;
    }

    /**
     * 
     * @param permalinkNavigationId
     *     The permalinkNavigationId
     */
    public void setPermalinkNavigationId(String permalinkNavigationId) {
        this.permalinkNavigationId = permalinkNavigationId;
    }

    /**
     * 
     * @return
     *     The permalink
     */
    public String getPermalink() {
        return permalink;
    }

    /**
     * 
     * @param permalink
     *     The permalink
     */
    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The createdAt
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The modifiedAt
     */
    public String getModifiedAt() {
        return modifiedAt;
    }

    /**
     * 
     * @param modifiedAt
     *     The modifiedAt
     */
    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    /**
     * 
     * @return
     *     The creatorUserId
     */
    public String getCreatorUserId() {
        return creatorUserId;
    }

    /**
     * 
     * @param creatorUserId
     *     The creatorUserId
     */
    public void setCreatorUserId(String creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    /**
     * 
     * @return
     *     The modifierUserId
     */
    public String getModifierUserId() {
        return modifierUserId;
    }

    /**
     * 
     * @param modifierUserId
     *     The modifierUserId
     */
    public void setModifierUserId(String modifierUserId) {
        this.modifierUserId = modifierUserId;
    }

    /**
     * 
     * @return
     *     The bylineUserId
     */
    public Object getBylineUserId() {
        return bylineUserId;
    }

    /**
     * 
     * @param bylineUserId
     *     The bylineUserId
     */
    public void setBylineUserId(Object bylineUserId) {
        this.bylineUserId = bylineUserId;
    }

    /**
     * 
     * @return
     *     The publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * 
     * @param publisher
     *     The publisher
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * 
     * @return
     *     The seoTitle
     */
    public String getSeoTitle() {
        return seoTitle;
    }

    /**
     * 
     * @param seoTitle
     *     The seoTitle
     */
    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    /**
     * 
     * @return
     *     The relatedItems
     */
    public Object getRelatedItems() {
        return relatedItems;
    }

    /**
     * 
     * @param relatedItems
     *     The relatedItems
     */
    public void setRelatedItems(Object relatedItems) {
        this.relatedItems = relatedItems;
    }

    /**
     * 
     * @return
     *     The syndicate
     */
    public String getSyndicate() {
        return syndicate;
    }

    /**
     * 
     * @param syndicate
     *     The syndicate
     */
    public void setSyndicate(String syndicate) {
        this.syndicate = syndicate;
    }

    /**
     * 
     * @return
     *     The mobileOnly
     */
    public String getMobileOnly() {
        return mobileOnly;
    }

    /**
     * 
     * @param mobileOnly
     *     The mobileOnly
     */
    public void setMobileOnly(String mobileOnly) {
        this.mobileOnly = mobileOnly;
    }

    /**
     * 
     * @return
     *     The navigation
     */
    public HashMap<String,Integer> getNavigation() {
        return navigation;
    }

    /**
     * 
     * @param navigation
     *     The navigation
     */
    public void setNavigation(HashMap<String,Integer> navigation) {
        this.navigation = navigation;
    }

    /**
     * 
     * @return
     *     The topics
     */
    public List<Object> getTopics() {
        return topics;
    }

    /**
     * 
     * @param topics
     *     The topics
     */
    public void setTopics(List<Object> topics) {
        this.topics = topics;
    }

    /**
     * 
     * @return
     *     The tags
     */
    public List<Object> getTags() {
        return tags;
    }

    /**
     * 
     * @param tags
     *     The tags
     */
    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    /**
     * 
     * @return
     *     The meta
     */
    public HashMap<String,String> getMeta() {
        return meta;
    }

    /**
     * 
     * @param meta
     *     The meta
     */
    public void setMeta(HashMap<String,String> meta) {
        this.meta = meta;
    }

    /**
     * 
     * @return
     *     The videos
     */
    public List<Object> getVideos() {
        return videos;
    }

    /**
     * 
     * @param videos
     *     The videos
     */
    public void setVideos(List<Object> videos) {
        this.videos = videos;
    }

    /**
     * 
     * @return
     *     The images
     */
    public List<Image> getImages() {
        return images;
    }

    /**
     * 
     * @param images
     *     The images
     */
    public void setImages(List<Image> images) {
        this.images = images;
    }

    /**
     * 
     * @return
     *     The geolocations
     */
    public List<Object> getGeolocations() {
        return geolocations;
    }

    /**
     * 
     * @param geolocations
     *     The geolocations
     */
    public void setGeolocations(List<Object> geolocations) {
        this.geolocations = geolocations;
    }

    /**
     * 
     * @return
     *     The relatedArticles
     */
    public List<Object> getRelatedArticles() {
        return relatedArticles;
    }

    /**
     * 
     * @param relatedArticles
     *     The relatedArticles
     */
    public void setRelatedArticles(List<Object> relatedArticles) {
        this.relatedArticles = relatedArticles;
    }

}
