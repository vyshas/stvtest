# README #

## Android application that displays "Top news" stories from Scottish TV API ! This is a personal project for learning purpose . I have used this as an opportunity to try GSON libraries and Google's Volley library. ##
*  Used volley library for data capture.
*  Used GSON library for parsing json .
*  For Image URL, used volley api for parsing json.
*  Caching mechanism of volley library was not used . Every request is directly taken from the network  .TODO CACHING

The data for that section is available, in JSON format, from this URL:

http://api.stv.tv/articles/?count=50&navigationLevelId=1218&orderBy=ranking+DESC%2C+createdAt+DESC&full=1&count=20
**NOTE: STV api is behaving strangely hence app will may work unpredictably. 

**
The app should grab the articles from this feed, and display them in a list view. Tapping one of these rows should open a web view, which will contain the HTML obtained from the ‘contentHTML’ property in the article data. The image should also be displayed above the HTML. 



APK will available at  https://www.dropbox.com/s/sgubc06np2k05ub/STVapp.apk?dl=0